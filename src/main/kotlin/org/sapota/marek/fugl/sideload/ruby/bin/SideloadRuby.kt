// Copyright 2021 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package org.sapota.marek.fugl.sideload.ruby.bin

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.convert
import com.github.ajalt.clikt.parameters.arguments.multiple
import com.github.ajalt.clikt.parameters.options.convert
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.clikt.parameters.types.choice
import kotlinx.coroutines.runBlocking
import org.sapota.marek.fugl.sideload.ruby.exceptions.RubyNotFoundException
import org.sapota.marek.fugl.sideload.ruby.executor.RubyExecutor
import java.io.File

enum class SideloadCommand {
    GEM,
    BUNDLER,
    RUN;

    companion object {
        fun displayValues(): Array<String> {
            return SideloadCommand
                .values()
                .map { it.name.toLowerCase() }
                .toTypedArray()
        }
    }
}

class SideloadRubyBin : CliktCommand() {
    val rubyPath: File? by option(
        "--ruby-path",
        "-r",
        help = "Path to ruby binary, uses PATH by default",
    )
        .convert("PATH") { File(it) }

    val gemPath: File by option(
        "--gem-path",
        "-g",
        help = "Target installation path"
    )
        .convert("PATH") { File(it) }
        .required()

    val command: SideloadCommand by argument(
        "command",
        help = "Command to run " +
            "(${SideloadCommand.displayValues().joinToString(", ")})",
    )
        .choice(*SideloadCommand.displayValues())
        .convert { param: String ->
            SideloadCommand.valueOf(param.toUpperCase())
        }

    val arguments: List<String> by argument(
        "arguments",
        help = "Command arguments",
    )
        .multiple()

    fun findRubyPath(): File {
        System.getenv("PATH").split(":").forEach { path ->
            val rubyPath = File("$path/ruby")
            if (rubyPath.exists()) {
                return rubyPath.parentFile
            }
        }
        throw RubyNotFoundException("Ruby binary not found")
    }

    override fun run() = runBlocking {
        val rubyBinPath: File = when {
            rubyPath == null -> findRubyPath()
            Regex("^.*/ruby(\\.ruby)?([0-9.]+)?$")
                .matches(rubyPath!!.path) -> rubyPath!!.parentFile
            else -> rubyPath!!
        }

        val executor = RubyExecutor(
            rubyBinPath,
            gemPath,
            cwd = if (command == SideloadCommand.BUNDLER) {
                null
            } else {
                gemPath
            },
        )
        when (command) {
            SideloadCommand.GEM -> SideloadRubyGemCommand(
                executor,
                arguments,
            ).run()
            SideloadCommand.BUNDLER -> SideloadRubyCommand(
                "bundler",
                executor,
                arguments,
            ).run()
            SideloadCommand.RUN -> SideloadRubyRunCommand(
                executor,
                arguments,
            ).run()
        }
    }
}

fun main(args: Array<String>) = SideloadRubyBin().main(args)
