#!/bin/bash
# Copyright 2021 Marek Sapota

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

set -euxo pipefail

CWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
JAR_PATH="${CWD}/../build/libs/fugl-sideload-ruby.jar"
MODULE="${1}"
INSTALL_PATH="/opt/fugl/sideload/ruby/${MODULE}"

mkdir -p "${INSTALL_PATH}"
cp "${MODULE}/Gemfile" "${INSTALL_PATH}/Gemfile"

pushd "${INSTALL_PATH}"

if [[ ! -d "${INSTALL_PATH}" ]]; then
    java -jar "${JAR_PATH}" -g "${INSTALL_PATH}" gem -- install bundler
fi

java -jar "${JAR_PATH}" -g "${INSTALL_PATH}" bundler -- install
java -jar "${JAR_PATH}" -g "${INSTALL_PATH}" bundler -- update
java -jar "${JAR_PATH}" -g "${INSTALL_PATH}" bundler -- clean --force
